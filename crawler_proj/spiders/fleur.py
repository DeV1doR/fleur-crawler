# -*- coding: utf-8 -*-
# import os
import datetime
from urlparse import urljoin
from urllib import urlencode
from scrapy import Spider, Request

from crawler_proj.utils import (
    get_simple_data_from_product, get_multiple_data_from_product,
    str2bool, statuses_ignore
)


class FleurSpider(Spider):

    name = "fleur"
    allowed_domains = ["fleur.ua"]
    handle_httpstatus_list = [301, 302, 400, 404, 405]
    custom_settings = {
        "FEED_EXPORT_FIELDS": [
            "name", "price", "min_delivery_time",
            "category", "sku", "is_avalilable"
        ],
        "CATEGORY_EXPORT_NAME": datetime.datetime.utcnow().strftime(
            "%Y-%m-%dT%H-%M-%S") + '.csv',
        "ITEM_PIPELINES": {
            "crawler_proj.pipelines.CategoryPipeline": 300,
        },
        "DOWNLOADER_MIDDLEWARES": {
            'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
            'crawler_proj.middlewares.RotateUserAgentMiddleware': 400,
        }
    }
    dict_start_urls = {
        u"Парфюмерия": "http://fleur.ua/parfyumeriya.html",

        # Not tested
        u"Косметика": "http://fleur.ua/kosmetika.html",
        u"Средства по уходу": "http://fleur.ua/sredstva-po-uhodu.html",
        u"Мужчины": "http://fleur.ua/mujchinyi.html",
        u"Подарки": "http://fleur.ua/podarki.html",
        u"Бренды": "http://fleur.ua/brands/",
    }

    def __init__(
        self, category="Парфюмерия", sub_category="",
        page_count=False, is_actual=False, use_proxy=False, *args, **kwargs
    ):
        """Description:
        :category - one category, which will be scraped, @str
        :sub_category - sub. category, which will be scraped, @str (
            if not needed, switch to @str "")
        :page_count - how many pages need to scrap, @int (
            if not needed, switch to @bool False)
        :is_actual - scrap only exists products, @bool
        :use_proxy - switch this to True for random proxies usage
         from the list, @bool
        """
        super(FleurSpider, self).__init__(*args, **kwargs)

        self.use_proxy = str2bool(use_proxy)

        if self.use_proxy:
            self.custom_settings["DOWNLOADER_MIDDLEWARES"].update({
                'scrapy.downloadermiddlewares.retry.RetryMiddleware': 500,
                'crawler_proj.middlewares.RandomProxy': 625,
                'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 750,
            })

        self.page_count = int(page_count)
        self.is_actual = str2bool(is_actual)
        try:
            self.category = category.decode('utf-8')
            self.sub_category = sub_category.decode('utf-8')
        except UnicodeDecodeError:
            self.category = category
            self.sub_category = sub_category

        try:
            self.start_urls = [self.dict_start_urls[self.category]]
        except KeyError:
            self.logger.info(
                'Category %s not found, perform search by all', self.category)
            self.start_urls = [v for v in self.dict_start_urls.itervalues()]

    @statuses_ignore(handle_httpstatus_list)
    def parse(self, response):
        """Parse categories given in start_urls.
        Default: http://fleur.ua/parfyumeriya.html
        """
        subcat_panel_xpath = ("//div[@class='mob-hide']/ul/li")
        if self.sub_category:
            subcat_link_xpath = subcat_panel_xpath + \
                ("/a[text()='%s']/@href" % self.sub_category)
            subcat_link = response.xpath(subcat_link_xpath).extract_first()
            if not subcat_link:
                msg = 'No such subcategory %s' % self.sub_category
                self.logger.error(msg)
                raise Exception(msg)
            yield Request(subcat_link, callback=self.parse_pag_and_count)
        else:
            subcat_links_xpath = subcat_panel_xpath + ("/a/@href")
            subcat_links = response.xpath(subcat_links_xpath).extract()
            for subcat_link in subcat_links:
                subsubcat_links_xpath = (
                    '//a[@href="%s"]/following-sibling::ul/li/a/@href' %
                    subcat_link)
                subsubcat_links = response \
                    .xpath(subsubcat_links_xpath).extract()
                if subsubcat_links:
                    for subsubcat_link in subsubcat_links:
                        yield Request(
                            subsubcat_link, callback=self.parse_pag_and_count)
                else:
                    yield Request(
                        subcat_link, callback=self.parse_pag_and_count)

    @staticmethod
    def _get_page_filter(sort_filter=None, **kwargs):
        """Set own sort filter.
        """
        if sort_filter is None:
            sort_filter = {
                "dir": "asc",
                "limit": 60,
                "order": "whats_new_sort"
            }
        sort_filter.update(**kwargs)
        return sort_filter

    def _get_paginator_count(self, response):
        """Calculate pagination count of one category.
        """
        if response.status in self.handle_httpstatus_list:
            return 0
        sort_filter = self._get_page_filter()
        product_count = response \
            .xpath("//span[@id='productsCount']/text()") \
            .re_first('\((\d+)\)')
        page_count = int(product_count) / float(sort_filter["limit"])
        _int_pag_c = int(product_count) / sort_filter["limit"]
        if page_count > _int_pag_c:
            page_count = _int_pag_c + 1
        page_count = int(page_count)
        if self.page_count and self.page_count <= page_count:
            page_count = self.page_count
        else:
            msg = "Pages for parse url %s not setuped, continue."
            self.logger.info(msg, response.url)
        return page_count

    def parse_pag_and_count(self, response):
        """Parse pages of one subcategory.
        """
        page_count = self._get_paginator_count(response)
        for p in xrange(1, page_count + 1):
            sort_filter = self._get_page_filter(p=p)
            next_url = urljoin(response.url, "?" + urlencode(sort_filter))
            yield Request(next_url, callback=self.parse_products_in_subcat)

    def parse_products_in_subcat(self, response):
        """Parse one paginated page of one subcategory.
        """
        items_xpath = ('//li[contains(@class, "item")]')
        exclude_urls = []
        if self.is_actual:
            exclude_xpath = items_xpath + \
                (
                    '/p[contains(@class, "out-of-stock")]'
                    '/preceding-sibling::a/@href'
                )
            exclude_urls = response.xpath(exclude_xpath).extract()

        products_urls_xpath = items_xpath + \
            ('/a[not(@class="link-wishlist")]/@href')

        # save breadcrumbs from paginated page
        breadcrumbs_xpath = (
            "//div[@class='breadcrumbs']/ul/li/child::node()/text()")
        _pag_breadcrumbs = response.xpath(breadcrumbs_xpath).extract()

        products_urls = response.xpath(products_urls_xpath).extract()
        products_urls = [
            url for url in products_urls if url not in exclude_urls]
        for product_url in products_urls:
            yield Request(
                product_url, callback=self.parse_product,
                meta={"_pag_breadcrumbs": _pag_breadcrumbs})

    def parse_product(self, response):
        """Parse data of one product.
        """
        products_sku = response \
            .xpath('//div[@class="product-info"]/div[@class="ham"]/@id') \
            .extract()

        if not products_sku:
            yield get_simple_data_from_product(response, self.is_actual)

        for product_sku in products_sku:
            yield get_multiple_data_from_product(
                response, product_sku, self.is_actual)
