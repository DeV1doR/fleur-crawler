# -*- coding: utf-8 -*-
import functools
import logging

from twisted.internet import defer
from scrapy import Request
from crawler_proj.items import FleurItemLoader


logger = logging.getLogger(__name__)


def check_fleur_logo(parse_method):
    """For random proxy support. Not Implemented.
    """

    @functools.wraps(parse_method)
    def wrapper(self, response):
        logo_link = response.xpath('//a[@class="logo"]/@href').extract_first()
        if logo_link == 'http://fleur.ua/':
            return parse_method(self, response)
        return Request(response.url, callback=parse_method, dont_filter=True)
    return wrapper


def statuses_ignore(list_of_status=None):

    if list_of_status is None:
        from scrapy.utils.project import get_project_settings
        settings = get_project_settings()
        list_of_status = settings.get('HTTPERROR_ALLOWED_CODES', [])

    def wrapper(parse_method):

        @functools.wraps(parse_method)
        def inner_wrapper(self, response):
            _def_args = (parse_method, self, response)
            if response.status in list_of_status:
                logger.info(
                    'Ignore status %s, continue.', response.status)
                _def_args = (lambda: None, )
            return defer.maybeDeferred(*_def_args)
        return inner_wrapper

    return wrapper


def str2bool(v):
    v = v.lower() if isinstance(v, str) else v
    return v in ("yes", "true", "t", "1", True)


def get_scrapy_args(cmd_args, list_of_args=None):
    if list_of_args is None:
        list_of_args = []

    args_props = [
        name for name in dir(cmd_args)
        if not name.startswith('_') and not name.startswith('__')]

    if not args_props:
        return ""

    for args_prop in args_props:
        cmd_arg = getattr(cmd_args, args_prop) or ""
        if cmd_arg:
            cmd_line = "-a%s=%s" % (args_prop, cmd_arg)
            list_of_args.append(cmd_line)

    return list_of_args


def get_simple_data_from_product(response, is_actual=False):
    itm_loader = FleurItemLoader(response=response)
    itm_loader.is_actual = is_actual

    itm_loader.add_value('_prod_url', response.url)
    itm_loader.add_value(
        '_pag_breadcrumbs', response.meta.get('_pag_breadcrumbs', []))

    product_category_xpath = ("//div[@class='breadcrumbs']/ul/li/a/text()")
    product_sku_xpath = ('//input[@id="pr_id"]/@value')
    product_name_xpath = ('//input[@id="pr_name"]/@value')
    product_price_xpath = (
        '//div[@class="info-block clearfix"]'
        '/div[@class="presence-box"]/div/span/span/text()')
    product_is_avl_xpath = (
        '//div[@class="info-block clearfix"]'
        '/div[@class="presence-box"]/p/@class')

    itm_loader.add_xpath('name', product_name_xpath)
    itm_loader.add_xpath('price', product_price_xpath, re="\d+")
    itm_loader.add_value('min_delivery_time', u"Не указано")
    itm_loader.add_xpath('category', product_category_xpath)
    itm_loader.add_xpath('category', product_name_xpath)
    itm_loader.add_xpath('sku', product_sku_xpath)
    itm_loader.add_xpath('is_avalilable', product_is_avl_xpath)

    return itm_loader.load_item()


def get_multiple_data_from_product(response, product_sku=None, is_actual=False):
    if not product_sku:
        return None

    itm_loader = FleurItemLoader(response=response)
    itm_loader.is_actual = is_actual

    itm_loader.add_value('_prod_url', response.url)
    itm_loader.add_value(
        '_pag_breadcrumbs', response.meta.get('_pag_breadcrumbs', []))

    product_category_xpath = ("//div[@class='breadcrumbs']/ul/li/a/text()")
    product_name_xpath = (
        '//input[@id="product_name_%s"]/@value' % product_sku)
    product_price_xpath = (
        '//div[@id="%s"]/div[@class="info-block clearfix"]'
        '/div[@class="presence-box"]'
        '/div/span/span/text()' % product_sku)
    product_is_avl_xpath = (
        '//div[@id="%s"]/div[@class="info-block clearfix"]'
        '/div[@class="presence-box"]'
        '/p/@class' % product_sku)

    itm_loader.add_xpath('name', product_name_xpath)
    itm_loader.add_xpath('price', product_price_xpath, re="\d+")
    itm_loader.add_value('min_delivery_time', u"Не указано")
    itm_loader.add_xpath('category', product_category_xpath)
    itm_loader.add_xpath('category', product_name_xpath)
    itm_loader.add_value('sku', product_sku)
    itm_loader.add_xpath('is_avalilable', product_is_avl_xpath)

    return itm_loader.load_item()
