### How to install ###

1. git clone https://bitbucket.org/dev1dor123/crawler-test
1. cd crawler-test/
1. virtualenv .env
1. source .env/bin/activate
1. (.env) pip install -r requirements.txt

Done.


### How to run job task for fleur spider ###

 * (.env) python job_crawl.py --repeat 3600 --category "Парфюмерия" -a

 - see python job_crawl.py --help for params explanation;
 - proxy - change proxy list on your own



### Description of fleur spider logic ###

1. Set up category name for parse;
1. With given category, spider will crawl subcategories and sub-subсategories (under the sub-category ex.: Парфюмерия > Парфюмерные наборы > Для женщин) of subcategory (if sub-subсategories exist);
1. After, spider searching how many products exists of each subcategory/sub-subсategory and construct links for pagination query with sorting (taking 60 prod per page (max));
1. Crawl paginated pages;
1. Go step by step through all products on the page (if --actual param, products with expected status ignores);
1. Getting data of each sku and save it to csv file by catalog;


### Screencast ###

https://www.youtube.com/watch?v=Lt_vWzF3CLI


### Logs ###

https://bitbucket.org/dev1dor123/crawler-test/raw/30c2a0704c656b86cbbcdeb969aebdd352d53945/logs/2016-05-04T17-41-32.log

### Catalog ###

https://bitbucket.org/dev1dor123/crawler-test/src/56f6fd51ea53e818301da280ac5cb2f7a2d160cf/loaded_data/fleur/?at=master