#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import time
import signal
import subprocess
import logging

from taskflow import task
from taskflow import engines
from taskflow.patterns import linear_flow

from scrapy.utils.log import configure_logging
from crawler_proj.utils import get_scrapy_args


logger = logging.getLogger("job_crawl")
logger.setLevel(logging.DEBUG)


class JobCrawlTask(task.Task):

    def execute(self, cmd_args):
        logger.info("Start execute <%s> spider...", self.name)
        scrapy_args = get_scrapy_args(cmd_args)

        if not scrapy_args:
            logger.info("Scrapy args not found, continue...")
        try:
            job_proc = subprocess.Popen(
                ['scrapy', 'crawl', self.name] + scrapy_args)
            job_proc.wait()
        except KeyError:
            raise Exception("Crawler <%s> doesn't exists.")
        except KeyboardInterrupt:
            job_proc.send_signal(signal.SIGINT)
            raise
        logger.info("Spider execution complete.")


def _argparse():
    description = """Job for fleur spider."""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-r", "--repeat", type=int, default=3600,
                        help="repeat time of job (in sec)")
    parser.add_argument("-c", "--category",
                        type=lambda s: unicode(s, 'utf8'),
                        dest="category", default="",
                        help="category to scrap")
    parser.add_argument("-sc", "--subcategory",
                        type=lambda s: unicode(s, 'utf8'),
                        dest="sub_category", default="",
                        help="subcategory to scrap")
    parser.add_argument("-p", "--pages", type=str,
                        dest="page_count", default="0",
                        help="pages of one subcategory")
    parser.add_argument("-a", "--actual", default=False,
                        dest="is_actual", action="store_true",
                        help="parse only actual category")
    parser.add_argument("-pr", "--use-proxy", default=False,
                        dest="use_proxy", action="store_true",
                        help="use for proxy crawling "
                        "(setup proxy/list.txt first)")
    args = parser.parse_args()
    return args


def main():
    configure_logging()
    args = _argparse()
    wf = linear_flow.Flow('crawler_job')
    wf.add(JobCrawlTask('fleur'))
    while True:
        try:
            logger.info("Running jobs...")
            e = engines.load(wf, store={"cmd_args": args})
            e.run()
            logger.info("Repeat after %s seconds", args.repeat)
            time.sleep(args.repeat)
        except KeyboardInterrupt:
            e.suspend()
            logger.info("Jobs...Done.")
            raise

if __name__ == '__main__':
    # subcat = Мужская парфюмерия
    # python job_crawl.py --repeat 3600 --category "Парфюмерия" -a --use-proxy
    main()
